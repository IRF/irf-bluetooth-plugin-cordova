var exec = require('cordova/exec');

/**
 * Constructor
 */
function IRFBluetooth(){
    /* Declare Constants Here */

}

IRFBluetooth.prototype.enroll = function(successCallback, errorCallback , config){
    if(config instanceof Array) {
        // do nothing
    } else {
        if(typeof(config) === 'object') {
            config = [ config ];
        } else {
            config = [];
        }
    }

    if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("IRFBluetooth.enroll failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("IRFBluetooth.enroll failure: success callback parameter must be a function");
        return;
    }

    exec(successCallback, errorCallback, 'IRFBluetoothCordova', 'enroll', config);
}

IRFBluetooth.prototype.validateSingleFP = function(successCallback, errorCallback , config){
    if(config instanceof Array) {
        // do nothing
    } else {
        if(typeof(config) === 'object') {
            config = [ config ];
        } else {
            config = [];
        }
    }

    if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("IRFBluetooth.enroll failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("IRFBluetooth.enroll failure: success callback parameter must be a function");
        return;
    }

    exec(successCallback, errorCallback, 'IRFBluetoothCordova', 'validatefp-single-finger', config);
}

IRFBluetooth.prototype.print = function(successCallback, errorCallback , printLines){
    var config = [JSON.stringify({
        "data": printLines
    })];
    if(config instanceof Array) {
        // do nothing
    } else {
        if(typeof(config) === 'object') {
            config = [ config ];
        } else {
            config = [];
        }
    }

    if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("IRFBluetooth.enroll failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("IRFBluetooth.enroll failure: success callback parameter must be a function");
        return;
    }

    exec(successCallback, errorCallback, 'IRFBluetoothCordova', 'print', config);
}

IRFBluetooth.prototype.validate = function(successCallback, errorCallback , config){
    if(config instanceof Array) {
        // do nothing
    } else {
        if(typeof(config) === 'object') {
            config = [ config ];
        } else {
            config = [];
        }
    }

    if (errorCallback == null) {
        errorCallback = function () {
        };
    }

    if (typeof errorCallback != "function") {
        console.log("IRFBluetooth.enroll failure: failure parameter not a function");
        return;
    }

    if (typeof successCallback != "function") {
        console.log("IRFBluetooth.enroll failure: success callback parameter must be a function");
        return;
    }

    console.log("Going to send validatefp request");

    exec(successCallback, errorCallback, 'IRFBluetoothCordova', 'validatefp', config);
}

var irfBluetooth = new IRFBluetooth();
module.exports = irfBluetooth;