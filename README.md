
# Installation

    plugman install --platform android --project ..\irf-btest\platforms\android --plugin ..\irf-bluetooth-plugin

# Removing
    plugman uninstall --platform android --project ..\irf-btest\platforms\android --plugin ..\irf-bluetooth-plugin


# Usage

## Enrollment

Enrollment captures fingerprints

    cordova.plugins.irfBluetooth.enroll(successCallback, errorCallback , config)

`successCallback` will be called once user submits the captured fingerprints. First argument to successcallback
 will be the result which have the following format

    {
        "code": 200,
        "status": "success",
        "data": [
            "Left Thumb fingerprint data in base64",
            "Left Index fingerprint data in base64",
            "Left Middle fingerprint data in base64",
            "Left Ring fingerprint data in base64",
            "Left Little fingerprint data in base64",
            "Right Thumb fingerprint data in base64",
            "Right Index fingerprint data in base64",
            "Right Middle fingerprint data in base64",
            "Right Ring fingerprint data in base64",
            "Right Little fingerprint data in base64"
        ]
    }