package com.irf.bluetooth;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.irf.irfbluetooth.activities.PrinterActivity;
import com.irf.irfbluetooth.activities.ValidateFingerprintActivity;
import com.irf.irfbluetooth.activities.ValidateFingerprintSingleActivity;
import com.irf.irfbluetooth.activities.CaptureFingerprintActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.lang.Exception;

/**
 * This class echoes a string called from JavaScript.
 */
public class IRFBluetoothCordova extends CordovaPlugin {

    private static final String TAG = "IRFBluetoothCordova";
    public CallbackContext callbackContext;

    static final int INTENT_CAPTURE_FINGERPRINT = 10;
    static final int INTENT_VERIFY_FINGERPRINT = 20;
    static final int INTENT_VERIFY_SINGLE_FINGERPRINT = 30;
    static final int INTENT_START_PRINT = 40;

    private static final String ENROLL_INTENT = "com.irf.irfbluetooth.ENROLL";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Log.d(TAG, "Inside IRFBluetoothCordova with action::" + action);
        this.callbackContext = callbackContext;
        if (action.equals("enroll")) {
            Log.d(TAG, "Inside action equals for enroll");
            this.startCaptureActivity();
            return true;
        } else if (action.equals("validatefp")) {
            try{
                Log.d(TAG, "Inside action equasl for validate");
                JSONObject data = args.getJSONObject(0);
                this.validateFingerprintActivity(data);
            } catch (Exception e){
                Log.e(TAG, "Some exeption ", e);

            }
            return true;
        } else if (action.equals("validatefp-single-finger")) {
            Log.d(TAG, "Inside action equals for enroll");
            String fpData = args.getString(0);
            this.validateSingleFingerActivity(fpData);
            return true;
        } else if (action.equals("print")) {
            Log.d(TAG, "Inside action equals for print");
            String printData = args.getString(0);
            this.printLines(printData);
            return true;
        }
        return false;
    }

    private void printLines(final String printData){
        Log.d(TAG, "Inside printLines");

        final CordovaPlugin that = this;

//        final String toValidateData = fpData;

        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                Context context = that.cordova.getActivity().getApplicationContext();
                Intent intent = new Intent(context, PrinterActivity.class);
                intent.putExtra("printer_data", printData);
                that.cordova.startActivityForResult(that, intent, INTENT_START_PRINT);
            }
        });
    }

    private void validateSingleFingerActivity(String fpData){
        Log.d(TAG, "Inside coolMethod1");

        final CordovaPlugin that = this;

        final String toValidateData = fpData;

        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                Context context = that.cordova.getActivity().getApplicationContext();
                Intent intent = new Intent(context, ValidateFingerprintSingleActivity.class);
                intent.putExtra("fpm-single-fp-data", toValidateData);
                that.cordova.startActivityForResult(that, intent, INTENT_VERIFY_SINGLE_FINGERPRINT);
            }
        });
    }

    private void startCaptureActivity(){
        Log.d(TAG, "Inside coolMethod1");

        final CordovaPlugin that = this;

        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                Context context = that.cordova.getActivity().getApplicationContext();
                Intent intent = new Intent(context, CaptureFingerprintActivity.class);
                that.cordova.startActivityForResult(that, intent, INTENT_CAPTURE_FINGERPRINT);
            }
        });
    }

    private void validateFingerprintActivity(JSONObject data){
        Log.d(TAG, "Inside validateFingerprintActivity");

        final JSONObject toConvertData = data;

        final CordovaPlugin that = this;

        cordova.getThreadPool().execute(new Runnable() {
            public void run() {
                Context context = that.cordova.getActivity().getApplicationContext();
                Intent intent = new Intent(context, ValidateFingerprintActivity.class);
                intent.putExtra("fpm-data-json", toConvertData.toString());
                that.cordova.startActivityForResult(that, intent, INTENT_VERIFY_FINGERPRINT);
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent){
        if (requestCode == INTENT_CAPTURE_FINGERPRINT){
            if (resultCode == Activity.RESULT_OK){
                try{
                    JSONObject out = new JSONObject();
                    out.put("status", "success");
                    out.put("code", 200);

                    String fpDataArray = intent.getStringExtra("fp-data-array");
                    JSONObject fpDataJsonObj = new JSONObject(fpDataArray);

                    out.put("data", fpDataJsonObj);
                    this.callbackContext.success(out);
                } catch (Exception e){
                    Log.d(TAG, "Error trying to send success response back to server", e);
                }

            }
        } else if (requestCode == INTENT_VERIFY_FINGERPRINT){
            if (resultCode == Activity.RESULT_OK) {
                try{
                    String status = intent.getStringExtra("status");
                    int code = intent.getIntExtra("code",0);
                    Log.d(TAG, "status is ::" + status + " code is::" + code);

                    JSONObject out = new JSONObject();
                    out.put("status", status);
                    out.put("code", code);

                    this.callbackContext.success(out);
                } catch (Exception e){
                    Log.e(TAG, "Error trying to send success response back to server", e);
                }
            }
        } else if (requestCode == INTENT_VERIFY_SINGLE_FINGERPRINT) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    String status = intent.getStringExtra("status");
                    int code = intent.getIntExtra("code",0);
                    Log.d(TAG, "Verify Single FP, status is ::" + status + " code is::" + code);

                    JSONObject out = new JSONObject();
                    out.put("status", status);
                    out.put("code", code);

                    this.callbackContext.success(out);
                } catch (Exception e){
                    Log.e(TAG, "Error trying to send success response back to server", e);
                }
            }
        }
    }
}