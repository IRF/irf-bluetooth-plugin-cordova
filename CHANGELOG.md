# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added
- Added validate single fingerprint verification
- Added skeleton for printer interface
- Added support for printer

### Changed
- Code fixes to improve response time from the device.
- Code redesign for standardizing bluetooth calls
- Added necessary changes to re initialize the streams on every device connections.
- Necessary changes for capturing only the fingerprint minutae

### Fixed
- Issue of module loading alrady done fixed.
- Fixed the issue with Validate last captured. Code wasnt there to handle this.

## 0.0.1 - 2016-03-30
### Added
- Necessary dependencies and files to make the plugin work.
